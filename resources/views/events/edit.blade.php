{{--
@section('action', 'Úprava podujatia ' . $event->title)
@extends('form')
--}}

@extends('root')

@section('content')
    <h1>@yield('Úprava')</h1>
    {!! Form::model($event, [
    'method' => 'PATCH',
    'route' => ['events.update', $event->id]
]) !!}
    <div class="form-group">
        {!! Form::label('eventName', 'Názov podujatia:') !!}
        {!! Form::text('eventName', isset($event) ? $event->title : null, ['class' => 'form-control', 'required' => true ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('eventDesc', 'Popis:') !!}
        {!! Form::text('eventDesc', isset($event) ? $event->description : null, ['class' => 'form-control', 'required' => true ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('eventPrice', 'Cena:') !!}
        {!! Form::number('eventPrice', isset($event) ? $event->price : null, ['class' => 'form-control', 'required' => true ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('eventDate', 'Dátum a čas:') !!}
        {!! Form::datetime('eventDate', isset($event) ? $event->dateTime : null, ['class' => 'form-control', 'required' => true ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('eventTown', 'Mesto:') !!}
        {!! Form::text('eventTown', isset($event) ? $event->town : null, ['class' => 'form-control', 'required' => true ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('eventPlace', 'Miesto:') !!}
        {!! Form::text('eventPlace', isset($event) ? $event->place : null, ['class' => 'form-control', 'required' => true ]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('eventImgUrl', 'Img url:') !!}
        {!! Form::text('eventImgUrl', isset($event) ? $event->ImgUrl : null, ['class' => 'form-control', 'required' => false ]) !!}
    </div>

    <div class="form-group">
        {!! Form::button('Upraviť', ['type' => 'submit']) !!}
    </div>

    {!! Form::close() !!}
@endsection
