<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $timestamps = false;

    private $id;
    protected $title;
    protected $description;
    protected $price;
    protected $dateTime;
    protected $town;
    protected $place;
    protected $imgUrl;

    protected $fillable = ['title', 'description', 'price', 'dateTime'];

    public function __construct()
    {
    }


}
