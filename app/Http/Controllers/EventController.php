<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Requests;
use phpDocumentor\Reflection\Types\True_;

class EventController extends Controller
{
    public function index() {
        $events = Event::all();
        return view('events.index', ['events' => $events]);
    }
    public function create() {
        return view('events.add');
    }
    public function store(Request $request) {
        $name = $request->input()['eventName'];
        $desc = $request->input()['eventDesc'];
        $price = $request->input()['eventPrice'];
        $dateT = $request->input()['eventDate'];
        $town = $request->input()['eventTown'];
        $place = $request->input()['eventPlace'];
        $imgUrl = $request->input()['eventImgUrl'];


        $event = new Event();

        $event->title = request('eventName');
        $event->description = request('eventDesc');
        $event->price = request('eventPrice');
        $event->dateTime = request('eventDateT');
        $event->town = request('eventTown');
        $event->place = request('eventPlace');
        $event->imgUrl = request('eventImgUrl');

        $event->save();
        return redirect('events');
    }
    public function show($id) {
        $event = Event::findOrFail($id);
        return view('events.show', ['event' => $event]);

    }
    public function edit($id) {
        $event = Event::findOrFail($id);
        return view('events.edit', ['event' => $event]);
    }
    public function update(Request $request, $id) {
        $event = Event::findOrFail($id);

        $event->title = request('eventName');
        $event->description = request('eventDesc');
        $event->price = request('eventPrice');
        $event->dateTime = request('eventDateT');
        $event->town = request('eventTown');
        $event->place = request('eventPlace');
        $event->imgUrl = request('eventImgUrl');


        $event->save();
        return redirect()->route('events.index');
    }
    public function destroy($id) {
        $event = Event::findOrFail($id);

        $event->delete();

        return redirect()->route('events.index');
    }
}
